﻿namespace WindowsFormsApplication1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel = new System.Windows.Forms.Panel();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.textSearch = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.группапроектаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.appDataSet = new WindowsFormsApplication1.appDataSet();
            this.организацияBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.проектBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.проектTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.ПроектTableAdapter();
            this.организацияTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.ОрганизацияTableAdapter();
            this.группа_проектаTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.Группа_проектаTableAdapter();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.зонаответственностиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.зона_ответственностиTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.Зона_ответственностиTableAdapter();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.отделорганизацииBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.отдел_организацииTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.Отдел_организацииTableAdapter();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.исполнительBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.исполнительTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.ИсполнительTableAdapter();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.оказаниеуслугиBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.оказание_услугиTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.Оказание_услугиTableAdapter();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.пользовательBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.пользовательTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.ПользовательTableAdapter();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.статус_выполненияTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.Статус_выполненияTableAdapter();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.услугаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.услугаTableAdapter = new WindowsFormsApplication1.appDataSetTableAdapters.УслугаTableAdapter();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.группапроектаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.организацияBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.проектBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.зонаответственностиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.отделорганизацииBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.исполнительBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.оказаниеуслугиBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.пользовательBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.услугаBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.textBox37);
            this.panel.Controls.Add(this.textBox36);
            this.panel.Controls.Add(this.textBox35);
            this.panel.Controls.Add(this.textBox34);
            this.panel.Controls.Add(this.textBox33);
            this.panel.Controls.Add(this.textBox32);
            this.panel.Controls.Add(this.textBox31);
            this.panel.Controls.Add(this.textBox30);
            this.panel.Controls.Add(this.label7);
            this.panel.Controls.Add(this.textBox29);
            this.panel.Controls.Add(this.textBox28);
            this.panel.Controls.Add(this.textBox27);
            this.panel.Controls.Add(this.textBox26);
            this.panel.Controls.Add(this.textBox25);
            this.panel.Controls.Add(this.textBox24);
            this.panel.Controls.Add(this.textBox23);
            this.panel.Controls.Add(this.textBox22);
            this.panel.Controls.Add(this.textBox21);
            this.panel.Controls.Add(this.textBox20);
            this.panel.Controls.Add(this.textBox19);
            this.panel.Controls.Add(this.textBox18);
            this.panel.Controls.Add(this.textBox17);
            this.panel.Controls.Add(this.textBox16);
            this.panel.Controls.Add(this.textBox15);
            this.panel.Controls.Add(this.textBox14);
            this.panel.Controls.Add(this.textBox13);
            this.panel.Controls.Add(this.textBox12);
            this.panel.Controls.Add(this.textBox11);
            this.panel.Controls.Add(this.textBox10);
            this.panel.Controls.Add(this.textBox9);
            this.panel.Controls.Add(this.textBox8);
            this.panel.Controls.Add(this.textBox7);
            this.panel.Controls.Add(this.textBox6);
            this.panel.Controls.Add(this.textBox5);
            this.panel.Controls.Add(this.label6);
            this.panel.Controls.Add(this.textBox4);
            this.panel.Controls.Add(this.textBox3);
            this.panel.Controls.Add(this.textBox2);
            this.panel.Controls.Add(this.textBox1);
            this.panel.Controls.Add(this.label4);
            this.panel.Controls.Add(this.label3);
            this.panel.Controls.Add(this.label2);
            this.panel.Controls.Add(this.label1);
            this.panel.Enabled = false;
            this.panel.Location = new System.Drawing.Point(12, 12);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(336, 160);
            this.panel.TabIndex = 0;
            // 
            // textBox10
            // 
            this.textBox10.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.оказаниеуслугиBindingSource, "Статус", true));
            this.textBox10.Location = new System.Drawing.Point(162, 111);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(141, 20);
            this.textBox10.TabIndex = 14;
            this.textBox10.Visible = false;
            // 
            // textBox9
            // 
            this.textBox9.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.организацияBindingSource, "Тип_организации", true));
            this.textBox9.Location = new System.Drawing.Point(162, 85);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(141, 20);
            this.textBox9.TabIndex = 13;
            this.textBox9.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.организацияBindingSource, "Телефон", true));
            this.textBox8.Location = new System.Drawing.Point(162, 59);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(141, 20);
            this.textBox8.TabIndex = 12;
            this.textBox8.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.организацияBindingSource, "Руководитель", true));
            this.textBox7.Location = new System.Drawing.Point(162, 33);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(141, 20);
            this.textBox7.TabIndex = 11;
            this.textBox7.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.организацияBindingSource, "Наименование", true));
            this.textBox6.Location = new System.Drawing.Point(162, 7);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(141, 20);
            this.textBox6.TabIndex = 10;
            this.textBox6.Visible = false;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(162, 113);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(141, 20);
            this.textBox5.TabIndex = 9;
            this.textBox5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Организация";
            // 
            // textBox4
            // 
            this.textBox4.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.проектBindingSource, "Организация", true));
            this.textBox4.Location = new System.Drawing.Point(162, 85);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(141, 20);
            this.textBox4.TabIndex = 7;
            this.textBox4.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.проектBindingSource, "Ответственный", true));
            this.textBox3.Location = new System.Drawing.Point(162, 59);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(141, 20);
            this.textBox3.TabIndex = 6;
            this.textBox3.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.проектBindingSource, "Группа", true));
            this.textBox2.Location = new System.Drawing.Point(162, 33);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(141, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.проектBindingSource, "Наименование", true));
            this.textBox1.Location = new System.Drawing.Point(162, 7);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(141, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Организация";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ответственный";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Группа";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Наименование";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(378, 71);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(83, 35);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(467, 71);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(83, 35);
            this.btnEdit.TabIndex = 2;
            this.btnEdit.Text = "Изменить";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(378, 112);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 35);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Отменить";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(467, 112);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(83, 35);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // textSearch
            // 
            this.textSearch.Location = new System.Drawing.Point(378, 19);
            this.textSearch.Name = "textSearch";
            this.textSearch.Size = new System.Drawing.Size(122, 20);
            this.textSearch.TabIndex = 5;
            this.textSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textSearch_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(511, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Поиск";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 178);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(547, 185);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown);
            // 
            // textBox11
            // 
            this.textBox11.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.группапроектаBindingSource, "Наименование", true));
            this.textBox11.Location = new System.Drawing.Point(162, 7);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(141, 20);
            this.textBox11.TabIndex = 15;
            this.textBox11.Visible = false;
            // 
            // textBox12
            // 
            this.textBox12.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.группапроектаBindingSource, "Руководитель", true));
            this.textBox12.Location = new System.Drawing.Point(162, 33);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(141, 20);
            this.textBox12.TabIndex = 16;
            this.textBox12.Visible = false;
            // 
            // textBox13
            // 
            this.textBox13.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.группапроектаBindingSource, "Статус_ответственности", true));
            this.textBox13.Location = new System.Drawing.Point(162, 59);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(141, 20);
            this.textBox13.TabIndex = 16;
            this.textBox13.Visible = false;
            // 
            // группапроектаBindingSource
            // 
            this.группапроектаBindingSource.DataMember = "Группа_проекта";
            this.группапроектаBindingSource.DataSource = this.appDataSet;
            // 
            // appDataSet
            // 
            this.appDataSet.DataSetName = "appDataSet";
            this.appDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // организацияBindingSource
            // 
            this.организацияBindingSource.DataMember = "Организация";
            this.организацияBindingSource.DataSource = this.appDataSet;
            // 
            // проектBindingSource
            // 
            this.проектBindingSource.DataMember = "Проект";
            this.проектBindingSource.DataSource = this.appDataSet;
            // 
            // проектTableAdapter
            // 
            this.проектTableAdapter.ClearBeforeFill = true;
            // 
            // организацияTableAdapter
            // 
            this.организацияTableAdapter.ClearBeforeFill = true;
            // 
            // группа_проектаTableAdapter
            // 
            this.группа_проектаTableAdapter.ClearBeforeFill = true;
            // 
            // textBox14
            // 
            this.textBox14.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.зонаответственностиBindingSource, "Статус_ответственности", true));
            this.textBox14.Location = new System.Drawing.Point(162, 7);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(141, 20);
            this.textBox14.TabIndex = 17;
            this.textBox14.Visible = false;
            // 
            // textBox15
            // 
            this.textBox15.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.зонаответственностиBindingSource, "Дата", true));
            this.textBox15.Location = new System.Drawing.Point(162, 33);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(141, 20);
            this.textBox15.TabIndex = 18;
            this.textBox15.Visible = false;
            // 
            // textBox16
            // 
            this.textBox16.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.зонаответственностиBindingSource, "Комментарий", true));
            this.textBox16.Location = new System.Drawing.Point(162, 59);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(141, 20);
            this.textBox16.TabIndex = 19;
            this.textBox16.Visible = false;
            // 
            // зонаответственностиBindingSource
            // 
            this.зонаответственностиBindingSource.DataMember = "Зона_ответственности";
            this.зонаответственностиBindingSource.DataSource = this.appDataSet;
            // 
            // зона_ответственностиTableAdapter
            // 
            this.зона_ответственностиTableAdapter.ClearBeforeFill = true;
            // 
            // textBox17
            // 
            this.textBox17.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.отделорганизацииBindingSource, "Наименование", true));
            this.textBox17.Location = new System.Drawing.Point(162, 7);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(141, 20);
            this.textBox17.TabIndex = 20;
            this.textBox17.Visible = false;
            // 
            // отделорганизацииBindingSource
            // 
            this.отделорганизацииBindingSource.DataMember = "Отдел_организации";
            this.отделорганизацииBindingSource.DataSource = this.appDataSet;
            // 
            // отдел_организацииTableAdapter
            // 
            this.отдел_организацииTableAdapter.ClearBeforeFill = true;
            // 
            // textBox18
            // 
            this.textBox18.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.отделорганизацииBindingSource, "Описание", true));
            this.textBox18.Location = new System.Drawing.Point(162, 33);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(141, 20);
            this.textBox18.TabIndex = 21;
            this.textBox18.Visible = false;
            // 
            // textBox19
            // 
            this.textBox19.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.отделорганизацииBindingSource, "Руководитель", true));
            this.textBox19.Location = new System.Drawing.Point(162, 59);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(141, 20);
            this.textBox19.TabIndex = 22;
            this.textBox19.Visible = false;
            // 
            // textBox20
            // 
            this.textBox20.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.отделорганизацииBindingSource, "Организация", true));
            this.textBox20.Location = new System.Drawing.Point(162, 85);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(141, 20);
            this.textBox20.TabIndex = 23;
            this.textBox20.Visible = false;
            // 
            // textBox21
            // 
            this.textBox21.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.исполнительBindingSource, "ФИО", true));
            this.textBox21.Location = new System.Drawing.Point(162, 7);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(141, 20);
            this.textBox21.TabIndex = 24;
            this.textBox21.Visible = false;
            // 
            // исполнительBindingSource
            // 
            this.исполнительBindingSource.DataMember = "Исполнитель";
            this.исполнительBindingSource.DataSource = this.appDataSet;
            // 
            // исполнительTableAdapter
            // 
            this.исполнительTableAdapter.ClearBeforeFill = true;
            // 
            // textBox22
            // 
            this.textBox22.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.исполнительBindingSource, "Должность", true));
            this.textBox22.Location = new System.Drawing.Point(162, 33);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(141, 20);
            this.textBox22.TabIndex = 25;
            this.textBox22.Visible = false;
            // 
            // textBox23
            // 
            this.textBox23.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.исполнительBindingSource, "Телефон", true));
            this.textBox23.Location = new System.Drawing.Point(162, 59);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(141, 20);
            this.textBox23.TabIndex = 26;
            this.textBox23.Visible = false;
            // 
            // textBox24
            // 
            this.textBox24.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.исполнительBindingSource, "Отдел_организации", true));
            this.textBox24.Location = new System.Drawing.Point(162, 85);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(141, 20);
            this.textBox24.TabIndex = 27;
            this.textBox24.Visible = false;
            // 
            // textBox25
            // 
            this.textBox25.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.оказаниеуслугиBindingSource, "Дата", true));
            this.textBox25.Location = new System.Drawing.Point(162, 7);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(141, 20);
            this.textBox25.TabIndex = 28;
            this.textBox25.Visible = false;
            // 
            // textBox26
            // 
            this.textBox26.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.оказаниеуслугиBindingSource, "Услуга", true));
            this.textBox26.Location = new System.Drawing.Point(162, 33);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(141, 20);
            this.textBox26.TabIndex = 29;
            this.textBox26.Visible = false;
            // 
            // textBox27
            // 
            this.textBox27.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.оказаниеуслугиBindingSource, "Организация", true));
            this.textBox27.Location = new System.Drawing.Point(162, 59);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(141, 20);
            this.textBox27.TabIndex = 30;
            this.textBox27.Visible = false;
            // 
            // textBox28
            // 
            this.textBox28.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.оказаниеуслугиBindingSource, "Описание", true));
            this.textBox28.Location = new System.Drawing.Point(162, 85);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(141, 20);
            this.textBox28.TabIndex = 31;
            this.textBox28.Visible = false;
            // 
            // textBox29
            // 
            this.textBox29.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.оказаниеуслугиBindingSource, "Трудочасы", true));
            this.textBox29.Location = new System.Drawing.Point(162, 137);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(141, 20);
            this.textBox29.TabIndex = 32;
            this.textBox29.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 140);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Организация";
            // 
            // оказаниеуслугиBindingSource
            // 
            this.оказаниеуслугиBindingSource.DataMember = "Оказание_услуги";
            this.оказаниеуслугиBindingSource.DataSource = this.appDataSet;
            // 
            // оказание_услугиTableAdapter
            // 
            this.оказание_услугиTableAdapter.ClearBeforeFill = true;
            // 
            // textBox30
            // 
            this.textBox30.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.пользовательBindingSource, "ФИО", true));
            this.textBox30.Location = new System.Drawing.Point(162, 7);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(141, 20);
            this.textBox30.TabIndex = 34;
            this.textBox30.Visible = false;
            // 
            // textBox31
            // 
            this.textBox31.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.пользовательBindingSource, "Должность", true));
            this.textBox31.Location = new System.Drawing.Point(162, 33);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(141, 20);
            this.textBox31.TabIndex = 35;
            this.textBox31.Visible = false;
            // 
            // textBox32
            // 
            this.textBox32.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.пользовательBindingSource, "Телефон", true));
            this.textBox32.Location = new System.Drawing.Point(162, 59);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(141, 20);
            this.textBox32.TabIndex = 36;
            this.textBox32.Visible = false;
            // 
            // textBox33
            // 
            this.textBox33.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.пользовательBindingSource, "Отдел_организации", true));
            this.textBox33.Location = new System.Drawing.Point(162, 85);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(141, 20);
            this.textBox33.TabIndex = 37;
            this.textBox33.Visible = false;
            // 
            // пользовательBindingSource
            // 
            this.пользовательBindingSource.DataMember = "Пользователь";
            this.пользовательBindingSource.DataSource = this.appDataSet;
            // 
            // пользовательTableAdapter
            // 
            this.пользовательTableAdapter.ClearBeforeFill = true;
            // 
            // textBox34
            // 
            this.textBox34.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "Статус", true));
            this.textBox34.Location = new System.Drawing.Point(162, 7);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(141, 20);
            this.textBox34.TabIndex = 38;
            this.textBox34.Visible = false;
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "Статус_выполнения";
            this.bindingSource1.DataSource = this.appDataSet;
            // 
            // статус_выполненияTableAdapter
            // 
            this.статус_выполненияTableAdapter.ClearBeforeFill = true;
            // 
            // textBox35
            // 
            this.textBox35.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.услугаBindingSource, "Наименование", true));
            this.textBox35.Location = new System.Drawing.Point(162, 7);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(141, 20);
            this.textBox35.TabIndex = 39;
            this.textBox35.Visible = false;
            // 
            // textBox36
            // 
            this.textBox36.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.услугаBindingSource, "Пользователь", true));
            this.textBox36.Location = new System.Drawing.Point(162, 33);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(141, 20);
            this.textBox36.TabIndex = 40;
            this.textBox36.Visible = false;
            // 
            // textBox37
            // 
            this.textBox37.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.услугаBindingSource, "Исполнитель", true));
            this.textBox37.Location = new System.Drawing.Point(162, 59);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(141, 20);
            this.textBox37.TabIndex = 41;
            this.textBox37.Visible = false;
            // 
            // услугаBindingSource
            // 
            this.услугаBindingSource.DataMember = "Услуга";
            this.услугаBindingSource.DataSource = this.appDataSet;
            // 
            // услугаTableAdapter
            // 
            this.услугаTableAdapter.ClearBeforeFill = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 372);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textSearch);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.panel);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.Shown += new System.EventHandler(this.Form2_Shown);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.группапроектаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.организацияBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.проектBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.зонаответственностиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.отделорганизацииBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.исполнительBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.оказаниеуслугиBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.пользовательBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.услугаBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView1;
      
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.BindingSource статусвыполненияBindingSource;
       
        private System.Windows.Forms.DataGridViewTextBoxColumn кодDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn значеиеDataGridViewTextBoxColumn;
        private appDataSet appDataSet;
        private System.Windows.Forms.BindingSource проектBindingSource;
        private appDataSetTableAdapters.ПроектTableAdapter проектTableAdapter;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.BindingSource организацияBindingSource;
        private appDataSetTableAdapters.ОрганизацияTableAdapter организацияTableAdapter;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.BindingSource группапроектаBindingSource;
        private appDataSetTableAdapters.Группа_проектаTableAdapter группа_проектаTableAdapter;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.BindingSource зонаответственностиBindingSource;
        private appDataSetTableAdapters.Зона_ответственностиTableAdapter зона_ответственностиTableAdapter;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.BindingSource отделорганизацииBindingSource;
        private appDataSetTableAdapters.Отдел_организацииTableAdapter отдел_организацииTableAdapter;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.BindingSource исполнительBindingSource;
        private appDataSetTableAdapters.ИсполнительTableAdapter исполнительTableAdapter;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.BindingSource оказаниеуслугиBindingSource;
        private appDataSetTableAdapters.Оказание_услугиTableAdapter оказание_услугиTableAdapter;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.BindingSource пользовательBindingSource;
        private appDataSetTableAdapters.ПользовательTableAdapter пользовательTableAdapter;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.BindingSource bindingSource1;
        private appDataSetTableAdapters.Статус_выполненияTableAdapter статус_выполненияTableAdapter;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.BindingSource услугаBindingSource;
        private appDataSetTableAdapters.УслугаTableAdapter услугаTableAdapter;
    }
}