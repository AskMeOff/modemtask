﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Услуга". При необходимости она может быть перемещена или удалена.
            this.услугаTableAdapter.Fill(this.appDataSet.Услуга);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Статус_выполнения". При необходимости она может быть перемещена или удалена.
            this.статус_выполненияTableAdapter.Fill(this.appDataSet.Статус_выполнения);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Пользователь". При необходимости она может быть перемещена или удалена.
            this.пользовательTableAdapter.Fill(this.appDataSet.Пользователь);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Оказание_услуги". При необходимости она может быть перемещена или удалена.
            this.оказание_услугиTableAdapter.Fill(this.appDataSet.Оказание_услуги);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Исполнитель". При необходимости она может быть перемещена или удалена.
            this.исполнительTableAdapter.Fill(this.appDataSet.Исполнитель);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Отдел_организации". При необходимости она может быть перемещена или удалена.
            this.отдел_организацииTableAdapter.Fill(this.appDataSet.Отдел_организации);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Зона_ответственности". При необходимости она может быть перемещена или удалена.
            this.зона_ответственностиTableAdapter.Fill(this.appDataSet.Зона_ответственности);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Группа_проекта". При необходимости она может быть перемещена или удалена.
            this.группа_проектаTableAdapter.Fill(this.appDataSet.Группа_проекта);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Организация". При необходимости она может быть перемещена или удалена.
            this.организацияTableAdapter.Fill(this.appDataSet.Организация);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "appDataSet.Проект". При необходимости она может быть перемещена или удалена.
            this.проектTableAdapter.Fill(this.appDataSet.Проект);
            
        }

        private void Form2_Shown(object sender, EventArgs e) // выбор требуемых элементов
        {
            switch (this.Text)
            {
                case "Проекты":
                    textBox1.Visible = true;
                    textBox2.Visible = true;
                    textBox3.Visible = true;
                    textBox4.Visible = true;
                    label1.Text = "Наименование";
                    label2.Text = "Группа";
                    label3.Text = "Ответственный";
                    label4.Text = "Организация";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = проектBindingSource;
                    break;
                case "Организации":
                    textBox6.Visible = true;
                    textBox7.Visible = true;
                    textBox8.Visible = true;
                    textBox9.Visible = true;
                    label1.Text = "Наименование";
                    label2.Text = "Руководитель";
                    label3.Text = "Телефон";
                    label4.Text = "Тип организации";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = организацияBindingSource;
                    break;
                case "Группы проектов":
                    textBox11.Visible = true;
                    textBox12.Visible = true;
                    textBox13.Visible = true;
                    label1.Text = "Наименование";
                    label2.Text = "Руководитель";
                    label3.Text = "Статус ответственности";
                    label4.Text = "";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = группапроектаBindingSource;
                    break;
                case "Зоны ответственности":
                    textBox14.Visible = true;
                    textBox15.Visible = true;
                    textBox16.Visible = true;
                    label1.Text = "Статус ответственности";
                    label2.Text = "Дата";
                    label3.Text = "Комментарий";
                    label4.Text = "";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = зонаответственностиBindingSource;
                    break;
                case "Отделы организаций":
                    textBox17.Visible = true;
                    textBox18.Visible = true;
                    textBox19.Visible = true;
                    textBox20.Visible = true;
                    label1.Text = "Наименование";
                    label2.Text = "Описание";
                    label3.Text = "Руководитель";
                    label4.Text = "Организация";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = отделорганизацииBindingSource;
                    break;
                case "Исполнители":
                    textBox21.Visible = true;
                    textBox22.Visible = true;
                    textBox23.Visible = true;
                    textBox24.Visible = true;
                    label1.Text = "ФИО";
                    label2.Text = "Должность";
                    label3.Text = "Телефон";
                    label4.Text = "Отдел организации";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = исполнительBindingSource;
                    break;
                case "Оказания услуг":
                    textBox25.Visible = true;
                    textBox26.Visible = true;
                    textBox27.Visible = true;
                    textBox28.Visible = true;
                    textBox10.Visible = true;
                    textBox29.Visible = true;
                    label1.Text = "Дата";
                    label2.Text = "Услуга";
                    label3.Text = "Организация";
                    label4.Text = "Описание";
                    label6.Text = "Статус";
                    label7.Text = "Трудочасы";
                    dataGridView1.DataSource = оказаниеуслугиBindingSource;
                    break;
                case "Пользователи":
                    textBox30.Visible = true;
                    textBox31.Visible = true;
                    textBox32.Visible = true;
                    textBox33.Visible = true;
                    label1.Text = "ФИО";
                    label2.Text = "Должность";
                    label3.Text = "Телефон";
                    label4.Text = "Отдел организации";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = пользовательBindingSource;
                    break;
                case "Статусы":
                    textBox34.Visible = true;
                    label1.Text = "Статус";
                    label2.Text = "";
                    label3.Text = "";
                    label4.Text = "";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = bindingSource1;
                    break;
                case "Услуги":
                    textBox35.Visible = true;
                    textBox36.Visible = true;
                    textBox37.Visible = true;
                    label1.Text = "Наименование";
                    label2.Text = "Пользователь";
                    label3.Text = "Исполнитель";
                    label4.Text = "";
                    label6.Text = "";
                    label7.Text = "";
                    dataGridView1.DataSource = услугаBindingSource;
                    break;
            }
        }
        

        private void dataGridView1_KeyDown(object sender, KeyEventArgs e) // удаление
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (MessageBox.Show("Уверены, что хотите удалить эту запись?", "Удалить?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    switch (this.Text)
                    {
                        case "Проекты":
                            проектBindingSource.RemoveCurrent();
                            break;
                        case "Организации":
                            организацияBindingSource.RemoveCurrent();
                            break;
                        case "Группы проектов":
                            группапроектаBindingSource.RemoveCurrent();
                            break;
                        case "Зоны ответственности":
                            зонаответственностиBindingSource.RemoveCurrent();
                            break;
                        case "Отделы организаций":
                            отделорганизацииBindingSource.RemoveCurrent();
                            break;
                        case "Исполнители":
                            исполнительBindingSource.RemoveCurrent();
                            break;
                        case "Оказания услуг":
                            оказаниеуслугиBindingSource.RemoveCurrent();
                            break;
                        case "Пользователи":
                            пользовательBindingSource.RemoveCurrent();
                            break;
                        case "Статусы":
                            bindingSource1.RemoveCurrent();
                            break;
                        case "Услуги":
                            услугаBindingSource.RemoveCurrent();
                            break;
                    }
                
            }
        }

        private void btnSave_Click(object sender, EventArgs e) // сохранить
        {
            switch (this.Text)
            {
                case "Проекты":

                    try
                    {
                        проектBindingSource.EndEdit();
                        проектTableAdapter.Update(this.appDataSet.Проект);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        проектBindingSource.ResetBindings(false);
                    }
                    break;
                case "Организации":

                    try
                    {
                        организацияBindingSource.EndEdit();
                        организацияTableAdapter.Update(this.appDataSet.Организация);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        организацияBindingSource.ResetBindings(false);
                    }
                    break;
                case "Группы проектов":

                    try
                    {
                        группапроектаBindingSource.EndEdit();
                        группа_проектаTableAdapter.Update(this.appDataSet.Группа_проекта);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        группапроектаBindingSource.ResetBindings(false);
                    }
                    break;
                case "Зоны ответственности":

                    try
                    {
                        зонаответственностиBindingSource.EndEdit();
                        зона_ответственностиTableAdapter.Update(this.appDataSet.Зона_ответственности);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        зонаответственностиBindingSource.ResetBindings(false);
                    }
                    break;
                case "Отделы организаций":

                    try
                    {
                        отделорганизацииBindingSource.EndEdit();
                        отдел_организацииTableAdapter.Update(this.appDataSet.Отдел_организации);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        отделорганизацииBindingSource.ResetBindings(false);
                    }
                    break;
                case "Исполнители":

                    try
                    {
                        исполнительBindingSource.EndEdit();
                        исполнительTableAdapter.Update(this.appDataSet.Исполнитель);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        исполнительBindingSource.ResetBindings(false);
                    }
                    break;
                case "Оказания услуг":

                    try
                    {
                        оказаниеуслугиBindingSource.EndEdit();
                        оказание_услугиTableAdapter.Update(this.appDataSet.Оказание_услуги);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        оказаниеуслугиBindingSource.ResetBindings(false);
                    }
                    break;
                case "Пользователи":

                    try
                    {
                        пользовательBindingSource.EndEdit();
                        пользовательTableAdapter.Update(this.appDataSet.Пользователь);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        пользовательBindingSource.ResetBindings(false);
                    }
                    break;
                case "Статусы":

                    try
                    {
                        bindingSource1.EndEdit();
                        статус_выполненияTableAdapter.Update(this.appDataSet.Статус_выполнения);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        bindingSource1.ResetBindings(false);
                    }
                    break;
                case "Услуги":

                    try
                    {
                        услугаBindingSource.EndEdit();
                        услугаTableAdapter.Update(this.appDataSet.Услуга);
                        panel.Enabled = false;
                        MessageBox.Show("Изменения сохранены!", "Сохранено", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Не существует такого поля в родительской таблице или пустое значение ключевого поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        услугаBindingSource.ResetBindings(false);
                    }
                    break;
            }
            
        }
        

        private void btnAdd_Click(object sender, EventArgs e) // добавление
        {
            panel.Enabled = true;
            switch (this.Text)
            {
                case "Проекты":
                    try
                    {
                        panel.Enabled = true;
                        textBox1.Focus();
                        this.appDataSet.Проект.AddПроектRow(this.appDataSet.Проект.NewПроектRow());
                        проектBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        проектBindingSource.ResetBindings(false);
                    }
                    break;
                case "Организации":
                    try
                    {
                        panel.Enabled = true;
                        textBox6.Focus();
                        this.appDataSet.Организация.AddОрганизацияRow(this.appDataSet.Организация.NewОрганизацияRow());
                        организацияBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        организацияBindingSource.ResetBindings(false);
                    }
                    break;
                case "Группы проектов":
                    try
                    {
                        panel.Enabled = true;
                        textBox11.Focus();
                        this.appDataSet.Группа_проекта.AddГруппа_проектаRow(this.appDataSet.Группа_проекта.NewГруппа_проектаRow());
                        группапроектаBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        группапроектаBindingSource.ResetBindings(false);
                    }
                    break;
                case "Зоны ответственности":
                    try
                    {
                        panel.Enabled = true;
                        textBox11.Focus();
                        this.appDataSet.Зона_ответственности.AddЗона_ответственностиRow(this.appDataSet.Зона_ответственности.NewЗона_ответственностиRow());
                        зонаответственностиBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        зонаответственностиBindingSource.ResetBindings(false);
                    }
                    break;
                case "Отделы организаций":
                    try
                    {
                        panel.Enabled = true;
                        textBox17.Focus();
                        this.appDataSet.Отдел_организации.AddОтдел_организацииRow(this.appDataSet.Отдел_организации.NewОтдел_организацииRow());
                        отделорганизацииBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        отделорганизацииBindingSource.ResetBindings(false);
                    }
                    break;
                case "Исполнители":
                    try
                    {
                        panel.Enabled = true;
                        textBox21.Focus();
                        this.appDataSet.Исполнитель.AddИсполнительRow(this.appDataSet.Исполнитель.NewИсполнительRow());
                        исполнительBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        исполнительBindingSource.ResetBindings(false);
                    }
                    break;
                case "Оказания услуг":
                    try
                    {
                        panel.Enabled = true;
                        textBox25.Focus();
                        this.appDataSet.Оказание_услуги.AddОказание_услугиRow(this.appDataSet.Оказание_услуги.NewОказание_услугиRow());
                        оказаниеуслугиBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        оказаниеуслугиBindingSource.ResetBindings(false);
                    }
                    break;
                case "Пользователи":
                    try
                    {
                        panel.Enabled = true;
                        textBox30.Focus();
                        this.appDataSet.Пользователь.AddПользовательRow(this.appDataSet.Пользователь.NewПользовательRow());
                        пользовательBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        пользовательBindingSource.ResetBindings(false);
                    }
                    break;
                case "Статусы":
                    try
                    {
                        panel.Enabled = true;
                        textBox34.Focus();
                        this.appDataSet.Статус_выполнения.AddСтатус_выполненияRow(this.appDataSet.Статус_выполнения.NewСтатус_выполненияRow());
                        bindingSource1.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        bindingSource1.ResetBindings(false);
                    }
                    break;
                case "Услуги":
                    try
                    {
                        panel.Enabled = true;
                        textBox35.Focus();
                        this.appDataSet.Услуга.AddУслугаRow(this.appDataSet.Услуга.NewУслугаRow());
                        услугаBindingSource.MoveLast();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        услугаBindingSource.ResetBindings(false);
                    }
                    break;
            }
        }


        private void textSearch_KeyPress(object sender, KeyPressEventArgs e) //поиск
        {
            switch (this.Text)
            {
                case "Проекты":
                    if (e.KeyChar == (char)13)
                    {
                        if(string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = проектBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Проект
                                        where q.Наименование.Contains(textSearch.Text) ||
                                        q.Группа.Contains(textSearch.Text) ||
                                        q.Ответственный.Contains(textSearch.Text) ||
                                        q.Организация.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                        //dataGridView1.ColumnCount = 5;
                    }
                    break;
                case "Организации":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = организацияBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Организация
                                        where q.Наименование.Contains(textSearch.Text) ||
                                        q.Руководитель.Contains(textSearch.Text) ||
                                        q.Телефон.Contains(textSearch.Text) ||
                                        q.Тип_организации.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Группы проектов":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = группапроектаBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Группа_проекта
                                        where q.Наименование.Contains(textSearch.Text) ||
                                        q.Руководитель.Contains(textSearch.Text) ||
                                        q.Статус_ответственности.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Зоны ответственности":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = зонаответственностиBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Зона_ответственности
                                        where q.Статус_ответственности.Contains(textSearch.Text) ||
                                        q.Дата.Contains(textSearch.Text) ||
                                        q.Комментарий.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Отделы организаций":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = отделорганизацииBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Отдел_организации
                                        where q.Наименование.Contains(textSearch.Text) ||
                                        q.Описание.Contains(textSearch.Text) ||
                                        q.Руководитель.Contains(textSearch.Text) ||
                                        q.Организация.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Исполнители":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = исполнительBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Исполнитель
                                        where q.ФИО.Contains(textSearch.Text) ||
                                        q.Должность.Contains(textSearch.Text) ||
                                        q.Телефон.Contains(textSearch.Text) ||
                                        q.Отдел_организации.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Оказания услуг":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = оказаниеуслугиBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Оказание_услуги
                                        where q.Дата.Contains(textSearch.Text) ||
                                        q.Услуга.Contains(textSearch.Text) ||
                                        q.Организация.Contains(textSearch.Text) ||
                                        q.Описание.Contains(textSearch.Text) ||
                                        q.Статус.Contains(textSearch.Text) ||
                                        q.Трудочасы.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Пользователи":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = пользовательBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Пользователь
                                        where q.ФИО.Contains(textSearch.Text) ||
                                        q.Должность.Contains(textSearch.Text) ||
                                        q.Телефон.Contains(textSearch.Text) ||
                                        q.Отдел_организации.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Статусы":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = статусвыполненияBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Статус_выполнения
                                        where q.Статус.Contains(textSearch.Text) 
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;
                case "Услуги":
                    if (e.KeyChar == (char)13)
                    {
                        if (string.IsNullOrEmpty(textSearch.Text))
                            dataGridView1.DataSource = услугаBindingSource;
                        else
                        {
                            var query = from q in this.appDataSet.Услуга
                                        where q.Наименование.Contains(textSearch.Text) ||
                                        q.Пользователь.Contains(textSearch.Text) ||
                                        q.Исполнитель.Contains(textSearch.Text)
                                        select q;
                            dataGridView1.DataSource = query.ToList();
                        }
                    }
                    break;

            }
        }

        private void btnEdit_Click(object sender, EventArgs e) //изменить
        {
            panel.Enabled = true;
        }

        private void btnCancel_Click(object sender, EventArgs e) // отмена
        {
            panel.Enabled = false;                
            проектBindingSource.ResetBindings(false);
            организацияBindingSource.ResetBindings(false);
            группапроектаBindingSource.ResetBindings(false);
            зонаответственностиBindingSource.ResetBindings(false);
            отделорганизацииBindingSource.ResetBindings(false);
            исполнительBindingSource.ResetBindings(false);
            оказаниеуслугиBindingSource.ResetBindings(false);
            пользовательBindingSource.ResetBindings(false);
            bindingSource1.ResetBindings(false);
            услугаBindingSource.ResetBindings(false);
        }
    }
}
